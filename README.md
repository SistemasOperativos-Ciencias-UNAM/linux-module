# `linux-module`

+ Para compilar, ejecutar `make all`
+ Para limpiar los objetos compilados, ejecutar `make clean`

## DKMS - Dynamic Kernel Module Support

Copiar el contenido del directorio `hello-0.1` en `/usr/src`

```sh
tonejito@debian:~/linux-module$ sudo cp -var hello-0.1 /usr/src/
[sudo] password for tonejito:           
‘hello-0.1’ -> ‘/usr/src/hello-0.1’
‘hello-0.1/dkms.conf’ -> ‘/usr/src/hello-0.1/dkms.conf’
‘hello-0.1/Makefile’ -> ‘/usr/src/hello-0.1/Makefile’
‘hello-0.1/hello.c’ -> ‘/usr/src/hello-0.1/hello.c’
```

Agregar el módulo a la lista de `dkms`

```sh
tonejito@debian:~/linux-module$ sudo dkms add -m hello -v 0.1

Creating symlink /var/lib/dkms/hello/0.1/source ->
                 /usr/src/hello-0.1

DKMS: add completed.
```

Compilar el módulo con `dkms`

```sh
tonejito@debian:~/linux-module$ sudo dkms build -m hello -v 0.1

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area....
make KERNELRELEASE=3.16.0-4-amd64 all KVERSION=3.16.0-4-amd64....
cleaning build area....

DKMS: build completed.
```

Instalar el módulo

```sh
tonejito@debian:~/linux-module$ sudo dkms install -m hello -v 0.1

hello:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/3.16.0-4-amd64/updates/dkms/

depmod....

DKMS: install completed.
```

Obtener información acerca del módulo

```sh
tonejito@debian:~/linux-module$ sudo modinfo hello
filename:       /lib/modules/3.16.0-4-amd64/updates/dkms/hello.ko
depends:        
vermagic:       3.16.0-4-amd64 SMP mod_unload modversions 
```

Cargar el módulo

```sh
tonejito@debian:~/linux-module$ sudo modprobe hello
```

Listar los módulos cargados

```sh
tonejito@debian:~/linux-module$ sudo lsmod | head -n 2
Module                  Size  Used by
hello                  12387  0 
```

Quitar el módulo

```sh
tonejito@debian:~/linux-module$ sudo modprobe -r hello
```

Desinstalar el módulo

```sh
tonejito@debian:~/linux-module$ sudo dkms remove -m hello -v 0.1 --all

-------- Uninstall Beginning --------
Module:  hello
Version: 0.1
Kernel:  3.16.0-4-amd64 (x86_64)
-------------------------------------

Status: Before uninstall, this module version was ACTIVE on this kernel.

hello.ko:
 - Uninstallation
   - Deleting from: /lib/modules/3.16.0-4-amd64/updates/dkms/
 - Original module
   - No original module was found for this module on this kernel.
   - Use the dkms install command to reinstall any previous module version.

depmod....

DKMS: uninstall completed.

------------------------------
Deleting module version: 0.1
completely from the DKMS tree.
------------------------------
Done.

```

# Referencias
+ <http://www.thegeekstuff.com/2013/07/write-linux-kernel-module/>
+ <http://www.linuxdevcenter.com/pub/a/linux/2007/07/05/devhelloworld-a-simple-introduction-to-device-drivers-under-linux.html?page=1>
+ <http://people.cs.pitt.edu/~jmisurda/teaching/cs449/valerie-henson-device-drivers-hello.pdf>
+ <http://www.linuxdevcenter.com/linux/2007/07/05/examples/hello_printk.tar.gz>
+ <http://www.linuxdevcenter.com/linux/2007/07/05/examples/hello_proc.tar.gz>
+ <http://www.linuxdevcenter.com/linux/2007/07/05/examples/hello_dev.tar.gz>

+ <http://linux.dell.com/dkms/>
+ <https://github.com/dell/dkms>
+ <https://wiki.kubuntu.org/Kernel/Dev/DKMSPackaging>
+ <http://www.dell.com/downloads/global/power/1q04-ler.pdf>
+ <http://www.linuxjournal.com/article/6896>
+ <https://wiki.debian.org/KernelDKMS>
+ <https://help.ubuntu.com/community/DKMS>
+ <https://help.ubuntu.com/community/Kernel/DkmsDriverPackage>
+ <https://wiki.centos.org/HowTos/BuildingKernelModules>
+ <https://wiki.archlinux.org/index.php/Dynamic_Kernel_Module_Support>
+ <http://xmodulo.com/build-kernel-module-dkms-linux.html>
